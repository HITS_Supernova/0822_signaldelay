# 0822 - Signal Delay

<table align="center">
    <tr>
    <td align="left" style="font-style:italic; font-size:12px; background-color:white">Explore the delays of space transmissions!<br>
        Send messages to the communication station on the opposite balcony. Notice the time it takes to transmit messages by looking at the clock. Is the delay noticeable for the Moon? How much time would it take to send a message to a planet of our neighboring star Proxima Centauri?</td>
    </tr>
</table>

The application lets the user experience the signal delay of space transmissions. When radio messages are transmitted, they travel at the fastest possible signal speed - the [speed of light](https://en.wikipedia.org/wiki/Speed_of_light) (approx. 300 000 km/s). Sending to the Moon (distance of approx. 380 000 km) it only takes a bit more than a second. For other planets in our Solar System, it takes minutes up to hours, depending on their current distance. However, if messages are sent to the planet orbiting around our closest neighbouring star, [Proxima Centauri b](https://en.wikipedia.org/wiki/Proxima_Centauri_b), it takes more than 4 year until the messages reaches that planet and it will take the same time until a possible response could return back (Proxima Centauri b is 4.2 light-years away).

With this application, the propagation times can be explored in a playful way, sending messages from two opposing stations back and forth. This may give an impression of the vastness of space. Transmission times are not only attached to the messages, but also made obvious by a fast-running clock. The propagation times are calculated according to the then-current position of the planets, so transmissions to Mars can take more or less time depending on the current distance of Mars to Earth. One station is permanently located on Earth and can select the message destination, the other station is at the respective location chosen (and will change its location when a new destination is selected on "Earth").

This application is used at the [ESO Supernova Planetarium and Visitor Centre](https://supernova.eso.org/?lang=en), Garching b. München. For more details about the project and how applications are run and managed within the exhibition please see [this link](https://gitlab.com/HITS_Supernova/overview).

## Requirements / How-To

The application should be run on a PC with a touch screen attached. The user interface is currently (and unfortunately) tailored for a FullHD resolution (1920 x 1080). Two stations with identical hardware are used, a server and a client. The server is permanently located on Earth, listens on TCP port 6001 for client connection, but also works stand-alone. The client tries to connect to the server (you need to pass the network address of the server to it) and then receives its chosen location from it. The application uses the Python [Kivy](https://kivy.org) framework for the graphical user interface. Furthermore it requires the `Twisted` and `pyephem` packages (full list, see `requirements.txt`). The startup in the exhibition system loads a virtual environment with the respective Python packages before the start (see `start.sh`). You may have to adapt or rewrite this to your needs.

Start the server:
```
python main.py -- --server
```

Starting the client:
```
python main.py -- --client --host 192.168.1.2
```
(use your actual server hostname or IP here).

Additional command line options:
```
python main.py -- --help
```

## Credits

This application was developed by the ESO Supernova team at [HITS gGmbH](https://www.h-its.org/en/).  
Idea and development by Volker Gaibler, HITS gGmbH.  

#### License

This project is licensed under the [GNU General Public License v3.0](LICENSE).

#### Image / Audio / Font Credits

* `img/earth.jpg`:  
  https://commons.wikimedia.org/wiki/File:Earth_Eastern_Hemisphere_2002.png  
  Authors: NASA/USGS EROS/MODIS/Robert Simmon, Reto Stöckli, "Blue Marble"  
  License: public domain  
  image size adjusted  

* `img/moon.jpg`:  
  https://commons.wikimedia.org/wiki/File:Full_Moon_Luc_Viatour.jpg  
  Author: Luc Viatour  
  Title: Full Moon Luc Viatour  
  License: https://creativecommons.org/licenses/by-sa/3.0/legalcode  
  image size adjusted  

* `img/mars.jpg`:  
  https://commons.wikimedia.org/wiki/File:Water_ice_clouds_hanging_above_Tharsis_PIA02653_black_background.jpg  
  Authors: NASA/JPL/MSSS  
  License: public domain  
  image size adjusted  

* `img/jupiter.jpg`:  
  https://commons.wikimedia.org/wiki/File:Jupiter.jpg  
  Authors: NASA/JPL/USGS  
  License: public domain  
  image size adjusted  

* `img/saturn.jpg`:
  https://commons.wikimedia.org/wiki/File:Saturn_-_High_Resolution,_2004.jpg  
  Authors: Mattias Malmer/Cassini Imaging Team (NASA)  
  License: public domain  
  image size adjusted  

* `img/proxima_b.jpg`:  
  https://www.eso.org/public/images/ann16056a/  
  Authors: ESO  
  Title: Screenshot of ESOcast 87  
  License: https://creativecommons.org/licenses/by/4.0/legalcode  
  image size adjusted and vignette effect added  

* `sound/beep-quindar.wav`:  
  uses the "Quindar tones" audio file https://commons.wikimedia.org/wiki/File:Quindar_tones.ogg  
  Author: Benscripps  
  License: https://creativecommons.org/licenses/by-sa/3.0/legalcode  

* `fonts/RobotoMono*`:  
  https://fonts.google.com/specimen/Roboto+Mono  
  Designer: Christian Robertson  
  Available under Apache License, Version 2.0  
