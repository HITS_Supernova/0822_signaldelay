mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
IMAGE_VERSION ?= v1.0

# site-specific settings
include ../0000_general/Hilbert/Makefile-local.inc
-include ../0000_general/Secrets/storage-api.inc

U = hits
APP = signaldelay

# default variables and targets
include ../0000_general/Hilbert/hilbert-docker-images/images/Makefile.inc

