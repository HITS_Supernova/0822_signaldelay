"""
    Image Buttons
    =============


"""

__all__ = ("BarBox", "BorderButton", "FillButton", "BarLabel", "BorderLabel", "FillLabel")
__version__ = '0.1'


from kivy.lang            import Builder
from kivy.uix.button      import Button
from kivy.uix.label       import Label
from kivy.properties      import StringProperty, ListProperty,\
                                 BooleanProperty, NumericProperty, ObjectProperty
from kivy.clock           import Clock
from kivy.logger          import Logger


Builder.load_string('''

### need clean kv definition where auto_scale: "both_lower" is set for BorderImage.
# maybe via "Redefining a widget's style" with "-Widget" syntax?
# just putting "autoscale" into kv defs does NOT work (property not exposed)!   siehe testimage.py


<BarButton,BarLabel,BorderButton,BorderLabel,FillButton,FillLabel>:
    # define understandable properties for frame and text, to be referenced further down
    frame_color: [1, 1, 1, 1]
    text_color: [0, 0, 0, 1]
    auto_scale: "off"
    hide: False
    height: 120

<BarButton,BarLabel>:
    background_normal: "img/barbox.png"
    background_down: "img/barbox_on.png"
    border: 69, 59, 69, 59
    background_color: root.frame_color

<BorderButton,BorderLabel>:
    background_normal: "img/borderbox.png"
    background_down: "img/borderbox_on.png"
    border: 59, 59, 59, 59
    background_color: root.frame_color

<FillButton,FillLabel>:
    background_normal: "img/fillbox.png"
    background_down: "img/fillbox_on.png"
    border: 59, 59, 59, 59
    background_color: root.frame_color

<FillButton>:
    background_color: [0, 0, 0, 0] if self.hide else root.frame_color
    color: [0, 0, 0, 0] if self.hide else root.text_color

#### TODO: Hack of making Buttons appear like Labels!!!
<BarLabel,BorderLabel,FillLabel>:
    background_down: self.background_normal
''')


class BarButton(Button):
    pass

class BorderButton(Button):
    pass

class FillButton(Button):
    pass


### TODO: Labels don't have the BorderImage!!!!   Using Button instead temporarily

class BarLabel(Button):
    pass

class BorderLabel(Button):
    pass

class FillLabel(Button):
    pass

if __name__ == "__main__":
    from kivy.app import App

    class SampleApp(App):
        def build(self):
            return FillButton(text="blubb", font_size=50)

    SampleApp().run()

