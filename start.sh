#!/bin/sh
# Start the application

echo "===== Signal Delay ====="
wd="$( dirname $(readlink -e $0) )"

echo "[+] Current environment:"
env | sort

# German keyboard connected
echo "[+] Setting keyboard layout to 'de' ..."
setxkbmap -layout de
sleep 1

# optionally disable some keys on keyboard
echo "[+] Xmodmap settings:"
if [ "$HILBERT_LOAD_XMODMAP" ] ; then
    echo "    Running 'xmodmap $HILBERT_LOAD_XMODMAP' ..."
    xmodmap "$HILBERT_LOAD_XMODMAP"
else
    echo "    HILBERT_LOAD_XMODMAP not defined. Nothing to do."
fi

echo "[+] HILBERT_SIGNALDELAYSERVER=$HILBERT_SIGNALDELAYSERVER"

# include hilbert-heartbeat module
export PYTHONPATH="/I/hilbert-heartbeat/client/python"
echo "[+] PYTHONPATH=$PYTHONPATH"

echo "[+] cd Python"
cd "$wd/Python"
echo "[+] Activating virtual environment ..."
. /opt/venv3/bin/activate

port=6001
if [ "$HILBERT_SIGNALDELAYSERVER" ] ; then
    echo "[+] starting as client ..."
    python main.py -- --client --port $port --host "$HILBERT_SIGNALDELAYSERVER"
else
    echo "[+] starting as server ..."
    python main.py -- --server --port $port --host ""
fi
