#!/usr/bin/python2
# -*- coding: utf-8 -*-

"""Communicator (Signal Delay)

tbd.

on desktop:
  python main.py -m inspector  --size 1280x800

to build for android:
- [buildozer init]
- buildozer android debug deploy
- adb logcat -s python
- and then run.....



"""

from __future__ import print_function, unicode_literals
from __future__ import division   # absolute_import

# global configuration, shared variables
import cfg

# install_twisted_rector must be called before importing  and using the reactor
# --> CommRadio uses twisted reactor, so need to integrate it here!
# then reactor runs within kivy event loop
from kivy.support import install_twisted_reactor
install_twisted_reactor()


import argparse

import time
import sys

import json

import kivy
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import StringProperty, NumericProperty, ObjectProperty
from kivy.metrics import sp, dp
from kivy.uix.behaviors import ToggleButtonBehavior
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.clock import Clock

from kivy.logger import Logger

from commradio import CommRadio
from commgui import CommGui
from action import Action

if cfg.heartbeat_active:
    import heartbeat

import gettext

kivy.require('1.10.1')



# version string
__version__ = "1.0"


###############################################################################
#
class CommunicatorApp(App):
    #pass
    #def build_config(self, config):
        ## built-in app configuration
        ## http://kivy.org/docs/api-kivy.app.html

        ## default values if pong.ini doesn't say otherwise
        #config.setdefaults('general', {
            #'verbose': 1,
	    #'frames_per_second': 60.,
            #})

    def __init__(self, **kwargs):
        super(CommunicatorApp, self).__init__(**kwargs)

        # attributes
        self._last_activity = time.time()


    ### app init
    #
    def build(self):
        ###twisted log.startLogging(sys.stdout)

        description, cfg.args = self.parse_arguments()
        cfg.progname = "{} - {}".format(description, "Server" if cfg.args.server else "Client")

        Logger.info("App: ========  {}  ========".format(cfg.progname))
        Logger.info("App: Command line arguments: {}".format(cfg.args))

        # make sure that reference to root widget is defined
        cfg.root = None

        # app title
        if sys.version_info.major < 3:
            self.title = cfg.progname.encode("ascii", "xmlcharrefreplace")
        else:
            self.title = cfg.progname

        cfg.action = Action()

        # setup communications "radio"
        # (hides the communication process)
        cfg.radio = CommRadio(is_server=cfg.args.server,
                              host=cfg.args.host,
                              port=cfg.args.port,
                              on_connection=cfg.action.connection,
                              on_message=cfg.action.message)

        # set different initial locations
        if cfg.args.server:
            cfg.locations_list[0]
        else:
            cfg.locations_list[1]

        # CommGui is graphical interface root widget
        cfg.root = CommGui()

        # hilbert heartbeat
        if cfg.heartbeat_active:
            self.heartbeat_setup_init()
            Clock.schedule_interval(lambda dt: self.periodic_health_check(), cfg.heartbeat_interval_sec)

        return cfg.root


    def parse_arguments(self):
        """Parsing of command line arguments."""

        # run with arguments:
        #python main.py -- --port 3000 --client


        parser = argparse.ArgumentParser(description="Signal Delay")

        # server or client
        exclusive_grp = parser.add_mutually_exclusive_group()
        exclusive_grp.add_argument("--server", action="store_true",
                            dest="server",
                            default=True,
                            help="Act as server (listen on host/port given)")
        exclusive_grp.add_argument("--client", action="store_false",
                            dest="server",
                            help="Act as client (connect to host/port given)")

        # server and client
        parser.add_argument("--port", action="store",
                            dest="port",
                            default="6001",
                            type=int,   # type=file
                            help="TCP port (default: 6001)")
        parser.add_argument("--host", action="store",
                            dest="host",
                            default="127.0.0.1", # emtpy: all
                            help="Host (default: 127.0.0.1, '' listens on all interfaces for server)")

        return (parser.description, parser.parse_args())


    ### Hilbert heartbeat: for monitoring the app's health status (optional)

    def heartbeat_setup_init(self):
        """Initialize hilbert heartbeat."""

        t = int(cfg.heartbeat_startup_sec * 1000)
        Logger.info("Heartbeat: hb_init({})".format(t))
        heartbeat.hb_init(t)


    def periodic_health_check(self):
        """Send hilbert heartbeat."""

        #global heartbeat_first_ping_shown
        t = int(cfg.heartbeat_interval_sec * 1000)
        if cfg.heartbeat_first_ping_shown:
            Logger.debug("Heartbeat: hb_ping({})".format(t))
        else:
            Logger.info("Heartbeat: hb_ping({})".format(t))
            Logger.info("Heartbeat: All further hb_ping() calls only shown on debug log level.")
            cfg.heartbeat_first_ping_shown = True
        heartbeat.hb_ping(t)




###############################################################################
#
if __name__ == '__main__':

    # https://docs.python.org/3.7/library/sys.html#sys.version_info
    #if sys.version_info.major < 3:
    #    Logger.error("App: Error. Requires python 3.")
    #    sys.exit(1)
    
    try:
        app = CommunicatorApp()
        app.run()
    except KeyboardInterrupt:
        pass
    except:
        raise
    finally:
        # close server?
        # app.comm.stop()
        pass
    

