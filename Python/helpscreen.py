# -*- coding: utf-8 -*-

"""
    HelpScreen
    ==========


"""

from __future__ import print_function, division, unicode_literals

from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.floatlayout import FloatLayout

from kivy.lang            import Builder
from kivy.uix.label       import Label
from kivy.properties      import StringProperty, ListProperty,\
                                 BooleanProperty, NumericProperty, ObjectProperty
from kivy.clock           import Clock
from kivy.logger          import Logger
import numpy as np
import datetime
import time

import cfg

from buttonbox import FillButton


Builder.load_string('''

<HelpScreen>:
    canvas.before:
        Color:
            rgba:
                0, 0, 0, 1
        Rectangle:
            size: root.width, root.height * (1 - 2 * root.grid_block_height)
            pos: self.x, root.height * root.grid_block_height

        Color:
            rgba:
                root.frame_color
        BorderImage:
            border: 59, 59, 59, 59
            source: "img/borderbox.png"
            size: root.width, root.height * (1 - 2 * root.grid_block_height)
            pos: self.x, root.height * root.grid_block_height

    # title
    Label:
        id: title_id
        text: root.title
        pos_hint: { "center_x": 0.5, "y": 1. - 10 * root.grid_block_height }
        size_hint: 0.9, 6. * root.grid_block_height
        font_size: root.font_size_title
        color: root.text_color

    # help text
    Label:
        id: text_id
        text: root.help_text
        #padding: 35, 35
        halign: "left"
        valign: "top"
        markup: True
        text_size: self.size
        pos_hint: { "center_x": 0.5, "y": 9. * root.grid_block_height }
        size_hint: 0.9, 32 * root.grid_block_height
        color: root.text_color

    # close button
    Button:
        pos_hint: { "center_x": 0.5, "y": 3 * root.grid_block_height }
        size_hint: 1.5 * 6 * root.grid_block_height, 6 * root.grid_block_height
        text: root.button_text
        font_size: root.font_size_button
        background_normal: "img/fillbox.png"
        background_down: "img/fillbox.png"
        border: 57, 57, 57, 57
        color: root.button_text_color
        background_color: root.button_color
        on_press: root.dismiss()
''')


class HelpScreen(FloatLayout):

    grid_block_height = NumericProperty(1/54.)
    """Grid units defined externally, e.g. 1/nblocky."""

    title = StringProperty("title")
    """Title text."""

    help_text = StringProperty("help_text")
    """Help text."""

    font_size_title = NumericProperty(40)
    """Font size of the title."""

    font_size_text = NumericProperty(20)
    """Font size of the help text."""

    font_size_button = NumericProperty(20)
    """Font size of close button."""

    text_color = ListProperty([0, 0, 1, 1])
    """rgba color of the text."""

    frame_color = ListProperty([1, 1, 0, 1])
    """Canvas background rgba color."""

    button_color = ListProperty([0, 0, 1, 1])
    """Color of close button."""

    button_text_color = ListProperty([0, 0, 1, 1])
    """Color of text in close button."""


    button_text = StringProperty("button_text")

    def dismiss(self):
        """Remove self."""
        Logger.debug("HelpScreen: ask parent to dismiss me")
        self.parent.remove_widget(self)


if __name__ == "__main__":
    from kivy.app import App

    class SampleApp(App):
        def build(self):
            return HelpScreen()

    SampleApp().run()
