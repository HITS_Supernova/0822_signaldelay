# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals
from __future__ import division   # absolute_import

import numpy as np

from kivy.properties import StringProperty, ListProperty
from kivy.logger import Logger
from kivy.clock import Clock
from kivy.utils import escape_markup

from buttonbox import BarLabel

# local configuration
import cfg




###############################################################################
class MessageList(BarLabel):
    """Message list widget, where past communication is displayed.

    Primary content is self.msg_list, from which a visual representation (widget text) is then derived.
    """

    msg_list = []

    def __init__(self, **kwargs):
        super(MessageList, self).__init__(**kwargs)

        Logger.debug("MessageList: initialized.")
        self.reset()

    def add_message(self, msg_dict):
        Logger.debug("MessageList: add_message {}".format(msg_dict))
        self.msg_list.append(msg_dict)
        self.update_text()
        # any need to purge old messages?

    def update_text(self):
        self.unrevealed_pending = False
        t = ""
        for m in self.msg_list:
           t += self.msg_to_text(m)
        tsplit = t.splitlines(True)

        # simplistic limit for the moment (limited by the label widget anyway)
        n_lines = 20
        if len(tsplit) > n_lines:
            self.text = "".join(tsplit[-n_lines:])
        else:
            self.text = t

        # unrevealed messages pending
        if self.unrevealed_pending:
            dt = 0.2
            Logger.info("MessageList: unrevealed pending. Scheduling update in {} seconds.".format(dt))
            Clock.schedule_once(lambda dt: self.update_text(), dt)

    def reset(self):
        self.msg_list = []
        self.update_text()

    def msg_to_text(self, msg):

        # local vs remote message formatting
        Logger.debug("msg: {}".format(msg))
        if msg["from_local"]:
            ft0 = "[color=" + cfg.message_color_local + "]"
            ft1 = "[color=" + cfg.message_color_local + "]"
        else:
            ft0 = "[color=" + cfg.message_color_remote + "]"
            ft1 = "[/color]"

        # don't show message if coming from remote and not yet arrive here yet
        receivetime = cfg.string_to_datetime64(msg["receivetime"])
        #delme np.timedelta64(int(1000. * cfg.animation_duration), "ms"),
        currenttime = cfg.clock.t
        if (not msg["from_local"]) and (receivetime > currenttime):
            self.unrevealed_pending = True
            #Logger.info("Messagelist: still has unrevealed message. receivetime={}, clock.t={}".format(
            #    receivetime, currenttime))
            #return ft0 + "<unrevealed message>" + ft1 + "\n"
            return ""


        # info formatting
        fi0 = "[size=15][color=#777777][i]"
        fi1 = "[/i][/color][/size]"

        # time delay
        delay = self.seconds_to_readable(msg["plustime"])

        # first show text
        format_string_text = "{ft0}{message}{ft1}"
        text = format_string_text.format(
            ft0=ft0,
            message=escape_markup(msg.get("message", "-/-")),
            ft1=ft1)

        # second show info line
        if cfg.lang == "de":
            format_string_info = "{fi0}[{sender} an {receiver}, {send_time}, Signallaufzeit {delay_value} {delay_unit}]{fi1}"
        else:
            format_string_info = "{fi0}[{sender} to {receiver}, {send_time}, signal propagation delay {delay_value} {delay_unit}]{fi1}"
        info = fi0 + format_string_info.format(
            fi0=fi0,
            sender=cfg.locations[msg["sender"]][cfg.lang],
            receiver=cfg.locations[msg["receiver"]][cfg.lang],
            send_time=msg["sendtime"].replace("T", " ")[:-4],
            delay_value=delay[0],
            delay_unit=delay[1],
            fi1=fi1,
            ).rjust(139, " ") + fi1

        return text + "\n" + info + "\n"

    def seconds_to_readable(self, secs):
        """Converts times given in seconds to human-readable units."""

        units = [31556952., 86400., 3600., 60., 1.]
        if cfg.lang == "de":
            unit_names = ["Jahre", "Tage", "Stunden", "Minuten", "Sekunden"]
        else:
            unit_names = ["years", "days", "hours", "minutes", "seconds"]
        for u, un in zip(units, unit_names):
            f = secs / u
            if f >= 1.:
                break
        f = round(f, 1)
        Logger.debug("MessageList: seconds_to_readable: {} {}".format(f, un))
        return (f, un)
