# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals
from __future__ import division   # absolute_import

from kivy.logger import Logger

# logger
#logger = logging.getLogger("server")

from twisted.internet.protocol import ServerFactory, ReconnectingClientFactory
from twisted.protocols.basic import NetstringReceiver
from twisted.protocols.basic import LineReceiver

import json

import cfg

###############################################################################
#

"""Message Types:

json dicts directly, or objects (initialize with params or the dictionary)

type: advance_time
    dt: some time spec

type: message [from user]
    ??? src: ...
    ??? dest: ...
    text: ...

type: notification [from system]
    text: ...

type: location_change
    src: ...
    dest: ...



TODO, VG, 03.04.2019:
- think of messages possible
- implement data exchange
- maybe CommProx not needed as Proxy, but rather as communication object (not extra abstraction)
- client server read and waiting for connection, but not blocking: continue as normal if not connected, except maybe show connection icon/text???
   --> dummy mode without communication (if none registered)?
   
- treat connection lost nicely???  for server, for client??? or don't care?


TODO:
    not so nice to have connectionMade and lineReceived callbacks defined twice and messy callback structure. Should eventually be improved!


"""


###############################################################################
#
class CommRadio(object):
    """Abstracts communications.

    Does simple pub-sub without the need to subscribe since only 2 participants.
    Could also just ignore networking for dummy mode.

    Main two methods are "publish_message" and "received_message". TODO update text.
    They send to everybody except self or are called once notified by others (but not for the 
    original sender). There is no "subscribe" because there is only this one "channel".

    """

    def __init__(self, is_server=True, host="127.0.0.1", port=6000,
                 on_connection=None, on_message=None):
        """
        on_connection: callback when connected
        on_message: callback when data is transmitted (received/transmitted), data as argument
        """
        self.is_server = is_server
        self.host = host
        self.port = port
        self.on_connection = on_connection
        self.on_message = on_message


        if self.is_server:
            Logger.info("CommRadio: Setting up TCP server ...")
            self.factory = CommServerFactory(on_connection=self.on_connection, on_received=self.received)
            from twisted.internet import reactor
            port = reactor.listenTCP(self.port,
                                self.factory,
                                interface=self.host)
            # server receives station number "0"
            cfg.station = 0
            Logger.info("CommRadio: Server mode: serving on {}".format(port.getHost()))


        else:
            Logger.info("CommRadio: Setting up TCP client ...")
            self.factory = CommClientFactory(on_connection=self.on_connection, on_received=self.received)
            from twisted.internet import reactor
            reactor.connectTCP(self.host,
                            self.port,
                            self.factory)
            Logger.info("CommRadio: Client mode: connecting to '{}' on port '{}'".format(
                self.host, self.port))



    ### high-level routines to notify participating machines (currently only 1 server + 1 client)
    #
    # Everything is transmitted through a json object
    #

    # send
    #
    def publish(self, msg={}):
        """Package and send messages, take care of callback routines.

        Also adds the "origin" key that identifies the original sender.

        msg: dictionary with all respective values.
        """

        # add origin info
        msg["origin"] = self.factory.station_id

        Logger.debug("CommRadio: publish dict {}".format(msg))
        msg_json = json.dumps(msg)
        Logger.debug("CommRadio: factory publish json '{}'".format(msg_json))
        self.factory.publish(msg_json)

        # add info about local origin to message
        msg["from_local"] = True
        Logger.debug("CommRadio: received: adding key from_local={}".format(msg["from_local"]))

        # callback:
        # processing of actions related to the message (BOTH incoming and outgoing!)
        self.on_message(msg)


    # receive
    #
    def received(self, line, proto):
        """Receive and process messages, take care of callbacks.

        This method is a *callback* when something (a line) is received on connection "protocol".
        """

        try:
            msg = json.loads(line)
        except ValueError:
            Logger.error("CommRadio: Could not decode as JSON object. '{}'".format(line))
            proto.transport.loseConnection()
            return

        Logger.info("CommRadio: lineReceived dict {}".format(msg))

        # add info about local origin to message
        #msg["from_local"] = (msg.get("origin", None) == self.factory.station_id)
        msg["from_local"] = False
        Logger.debug("CommRadio: received: adding key from_local={}".format(msg["from_local"]))

        # pass to callback: 
        # processing of actions related to the message (BOTH incoming and outgoing!)
        self.on_message(msg)





###############################################################################
#
class CommProtocol(LineReceiver):
    """Protocol is common to server and client.

    Once protocol has been created (i.e. connection established), protocol is in charge of specific connection, not factory."""

    #delimiter = b'\r\n'
    delimiter = b'\n'


    def connectionMade(self):
        """Called when a connection is made."""

        Logger.info("CommProtocol: connectionMade with remote peer {}".format(self.transport.getPeer()))
        Logger.info("CommProtocol: register connection with my factory {} ...".format(self.factory))
        self.factory.register_connection(self)

        # hand over to factory
        self.factory.protoConnectionMade(self)


    def lineReceived(self, line):
        Logger.debug("CommProtocol: lineReceived {}".format(line))

        self.factory.protoLineReceived(line, self)


    def connectionLost(self, reason):
        """Connection lost:

        Is actually called for both server and client case.
        However, only interesting for server case since client case with reconnection 
        is handled by client factory.
        """

        Logger.info("CommProtocol: connectionLost, reason: {}".format(reason.getErrorMessage()))
        Logger.info("CommProtocol: unregister my connection with my factory {} ...".format(self.factory))
        self.factory.unregister_connection(self)






class CommFactoryMixin(object):
    """Mix-in that adds to both ServerFactory and ClientFactory."""

    station_id = None


    conns = []
    server = None
    counter = 0
    greeter_string = "hello v1 - your station_id is "

    def register_connection(self, proto):
        self.conns.append(proto)
        Logger.info("CommFactoryMixin: registered connection {}".format(
            proto.transport.getPeer()))
        Logger.info("CommFactoryMixin: now have {} connections".format(
            len(self.conns)))

    def unregister_connection(self, proto):
        self.conns.remove(proto)
        Logger.info("CommFactoryMixin: unregistered connection {}".format(
            proto.transport.getPeer()))

    def publish(self, msg_string):

        # encode string as bytes
        msg_bytes = msg_string.encode()

        for proto in self.conns:
            Logger.info("CommFactoryMixin: sending to client {}".format(proto.transport.getPeer()))
            proto.sendLine(msg_bytes)

    def set_station_id(self, my_type):
        """Sets the station_id.

        station_id == 1: server
                   == 0: non-configured client
                   >= 2: configured client

        is_server is set for convenience.
        """

        if my_type == "server":
            self.is_server = True
            self.station_id = 1
        else:
            # will be assigned by server on connect
            self.is_server = False
            self.station_id = 0


    def protoConnectionMade(self, proto):

        Logger.info("CommFactoryMixin: protoConnectionMade. is_server={}, station_id={}".format(
            self.is_server, self.station_id))

        # server assigns station_id to connecting client
        if self.is_server:
            self.server_assign_station_id(proto)

            # on_connection callback here only for server
            if self.on_connection:
                self.on_connection(proto)


    def protoLineReceived(self, line_bytes, proto):
        """Received data from protocol. Data relayed to other clients."""

        # convert bytes to string
        line_string = line_bytes.decode()

        # clients receive station_id from server on first receive
        # return without connection callbacks because still part of setup procedure
        if self.station_id == 0:
            self.client_get_station_id(line_string, proto)

            if self.on_connection:
                self.on_connection(proto)
            return

        # on_connection callback here only for client
        if self.on_received:
            self.on_received(line_string, proto)


    def server_assign_station_id(self, proto):
        """Server: assign station_id to clients when they connect."""

        self.counter += 1
        msg = self.greeter_string + str(self.counter + 1)
        proto.sendLine(msg.encode())

    def client_get_station_id(self, line, proto):
        """Clients: get station_id when connection is made, assigned by server."""

        if not line.startswith(self.greeter_string):
            Logger.warning("CommFactoryMixin: Received invalid greeter string: '{}'. Dropping connection.".format(line))
            proto.transport.loseConnection()
            return

        try:
            idx = len(self.greeter_string.split())
            self.station_id = int(line.split()[idx])
        except ValueError:
            Logger.warning("CommFactoryMixin: Received invalid station_id: '{}'. Dropping connection.".format(line))
            proto.transport.loseConnection()
            return

        Logger.info("CommFactoryMixin: client with station_id={} (assigned by server)".format(self.station_id))






###############################################################################
#   only standard factory, set callbacks, reconnect below:
###############################################################################

###############################################################################
#
class CommServerFactory(ServerFactory, CommFactoryMixin):
    """Server factory."""

    protocol = CommProtocol

    def __init__(self, on_connection=None, on_received=None):

        self.set_station_id("server")

        # callbacks
        self.on_connection = on_connection
        self.on_received = on_received

        Logger.info("CommServerFactory: self is {}".format(self))
        Logger.info("CommServerFactory: station_id={}".format(self.station_id))

###############################################################################
#
class CommClientFactory(ReconnectingClientFactory, CommFactoryMixin):
    """Reconnecting client factory."""

    protocol = CommProtocol

    def __init__(self, on_connection=None, on_received=None):

        self.set_station_id("client")

        # callbacks
        self.on_connection = on_connection
        self.on_received = on_received

        Logger.info("CommClientFactory: self is {}".format(self))
        Logger.info("CommClientFactory: station_id={}".format(self.station_id))

    def buildProtocol(self, *args, **kw):
        prot = ReconnectingClientFactory.buildProtocol(self, *args, **kw)
        Logger.info("CommClientFactory: Connected. Resetting reconnect delay.")
        self.resetDelay()
        return prot

    # required reconnection methods
    #

    def clientConnectionFailed(self, connector, reason):
        self.station_id = 0
        Logger.info("CommClientFactory: clientConnectionFailed: {}".format(reason.getErrorMessage()))
        ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)

    def clientConnectionLost(self, connector, reason):
        """Connection to client was shut down."""

        self.station_id = 0
        Logger.info("CommClientFactory: clientConnectionLost: {}".format(reason.getErrorMessage()))
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

