"""
    CommLink
    ========


"""

__all__ = ('CommLink')
__version__ = '0.1'


from kivy.lang            import Builder
from kivy.uix.label       import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.properties      import StringProperty, ListProperty,\
                                 BooleanProperty, NumericProperty, ObjectProperty
from kivy.clock           import Clock
from kivy.logger          import Logger
from kivy.core.audio      import SoundLoader
import numpy as np
import datetime
import time

import cfg

from buttonbox import BarLabel

Builder.load_string('''

<CommLink>:
    canvas.before:
        Color:
            rgba:
                root.frame_color
        BorderImage:
            border: 69, 59, 69, 59
            source: "img/barbox.png"
            size: self.size
            pos: self.pos

        # background line
        Line:
            width: 1
            points: [root.pos[0]+root.x_link*root.size[0], root.pos[1]+root.y_link_local*root.size[1], root.pos[0]+root.x_link*root.size[0], root.pos[1]+root.y_link_remote*root.size[1]]
            dash_length: 2
            dash_offset: 8

        # transmission signal
        Line:
            width: 3
            points: [root.pos[0]+root.x_link*root.size[0], root.y_signal_0, root.pos[0]+root.x_link*root.size[0], root.y_signal_1]

    Image:
        size_hint: 0.8, 0.05
        pos_hint: { "center_x": 0.5, "center_y": root.y_remote - 0.1 }
        source: root.remote_image
        mipmap: True

    Label:
        size_hint: 0.8, 0.1
        pos_hint: { "center_x": 0.5, "center_y": root.y_remote }
        text: root.remote_text
        color: root.text_color
        font_size: root.font_size
        markup: True
        text_size: self.size
        halign: "center"
        valign: "center"

    Image:
        size_hint: 0.8, 0.05
        pos_hint: { "center_x": 0.5, "center_y": root.y_local + 0.1 }
        source: root.local_image
        mipmap: True

    Label:
        size_hint: 0.8, 0.15
        pos_hint: { "center_x": 0.5, "center_y": root.y_local }
        text: root.local_text
        color: root.text_color
        font_size: root.font_size
        markup: True
        text_size: self.size
        halign: "center"
        valign: "center"

''')


###############################################################################
class CommLink(FloatLayout):
    """Visualizes the link from local to remote."""

    text = StringProperty("")
    """Text content."""

    font_size = NumericProperty(20)
    """Font size of the labels."""

    text_color = ListProperty([1, 1, 0, 1])
    """rgba color of the label text."""

    frame_color = ListProperty([1, 1, 0, 1])
    """Canvas background image rgba color."""

    y_local = NumericProperty(0.15)
    """Y coordinate of "local", relative to height."""

    y_remote = NumericProperty(0.9)
    """Y coordinate of "remote", relative to height."""

    x_link = NumericProperty(0.5)
    y_link_remote = NumericProperty(0.8)
    y_link_local = NumericProperty(0.25)
    """Local - to remote distance line, in relative width or height coordinates."""

    y_signal_0 = NumericProperty(0.)
    y_signal_1 = NumericProperty(0.)
    """Signal line, in pixel coordinates."""

    remote_text = StringProperty("REMOTE")
    local_text = StringProperty("LOCAL")
    """Label text for remote and local."""

    remote_image = StringProperty("img/earth.jpg")
    local_image = StringProperty("img/jupiter.jpg")
    """Image file path for remote and local."""


    def __init__(self, **kwargs):
        super(CommLink, self).__init__(**kwargs)
        self._clock_animation = None
        self._pending_steps = []

        # sound
        if cfg.use_sound:
            try:
                self.sound_send = SoundLoader.load(cfg.soundfile["send"])
                self.sound_send.volume = 1.0
                self.sound_send.loop = False
                self.sound_receive = SoundLoader.load(cfg.soundfile["receive"])
                self.sound_receive.volume = 1.0
                self.sound_receive.loop = False
            except:
                Logger.error("Action: Loading sound failed. Deactivated!!!")
                cfg.use_sound = False


    def update(self):
        here_localized = "\n(hier)" if cfg.lang == "de" else "\n(here)"
        self.local_text = "[color={}]{}[/color]{}".format(
            cfg.message_color_local, cfg.locations[cfg.local][cfg.lang], here_localized)
        self.remote_text = "[color={}]{}[/color]{}".format(
            cfg.message_color_remote, cfg.locations[cfg.remote][cfg.lang], "")
        self.local_image = cfg.locations[cfg.local]["image"]
        self.remote_image = cfg.locations[cfg.remote]["image"]

        # TODO: make nicer
        self.text = cfg.remote + "\n\n^\n|\n\n" + cfg.local

    def animate_message_to(self, where, steps=111, duration=cfg.animation_duration, line_length=0.03):
        Logger.debug("CommLink: Animating message to '{}' ...".format(where))

        # sound with animation
        # TODO: play when animation starts at 0 or 1 ????
        if cfg.use_sound:
            if where == "remote":
                self.sound_send.play()
            elif where == "local":
                self.sound_receive.play()

        if where == "remote":
            for i in range(1, (steps-1) + 1, 1):
                y1 = float(i) / (steps-1)
                y0 = max(y1 - line_length, 0.)
                self._pending_steps.append((y0, y1))
            self._pending_steps.append((1., 1.))
        elif where == "local":
            for i in range((steps-1) - 1, -1, -1):
                y0 = float(i) / (steps-1)
                y1 = min(y0 + line_length, 1.)
                self._pending_steps.append((y0, y1))
            self._pending_steps.append((0., 0.))
        #Logger.debug("CommLink: pending steps: {}".format(self._pending_steps))
        if not self._clock_animation:
            self._clock_animation = Clock.schedule_interval(self._make_animation, 1.*duration/steps)

    def _make_animation(self, dt):
        if self._pending_steps:
            step = self._pending_steps.pop(0)
            # scale to vertical link range and to absolute widget coordinates
            self.y_signal_0 = self.pos[1] + self.size[1] * (step[0] * (self.y_link_remote - self.y_link_local) + self.y_link_local)
            self.y_signal_1 = self.pos[1] + self.size[1] * (step[1] * (self.y_link_remote - self.y_link_local) + self.y_link_local)
        else:
            Logger.info("CommLink: Animation done.")
            self._clock_animation.cancel()
            self._clock_animation = None

    #def update_signal_line(self, *args):
    #    Logger.info("CommLink: update_signal_line {}".format(args))

    #def on_pos(self, *args):
    #    self.update_signal_line()

    #def on_size(self, *args):
    #    self.update_signal_line()





if __name__ == "__main__":
    from kivy.app import App

    class SampleApp(App):
        def build(self):
            w = CommLink()
            def send_message(dt):
                print("to remote")
                w.animate_message_to("remote")
            def recv_message(dt):
                print(" to local")
                w.animate_message_to("local")
            Clock.schedule_once(send_message, 1.)
            Clock.schedule_once(recv_message, 1.5)
            return w

    SampleApp().run()

