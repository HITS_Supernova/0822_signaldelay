# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals
from __future__ import division   # absolute_import

from kivy.logger import Logger

import cfg
import numpy as np
import ephem    # pip install pyephem


class Space(object):

    def __init__(self):
        self.const_au = 149597870700. # astronomical unit (AU), in m
        self.const_c = 299792458. # speed of light, in m/s
        self.const_c_aus = self.const_c / self.const_au    # speed of light, in AU/s

    def get_signal_time(self, source, destination, date):
        """Calculate signal travel time from source to destination."""

        Logger.debug("Space: get_signal_time currently DUMMY values!")

        loc_source = self.get_heliocentric_loc(source, date)
        loc_destination = self.get_heliocentric_loc(destination, date)
        d = np.linalg.norm(loc_destination - loc_source)   # in AU
        Logger.debug("Space: distance {} to {} on {} is {} AU".format(source, destination, date, d))

        dt = d / self.const_c_aus    # in s
        Logger.debug("Space: get_signal_time = {} sec".format(dt))
        return dt

    def get_heliocentric_loc(self, body, date):
        """Get heliocentric, ecliptical cartesian coordinates, lengths in AU.

        TODO: Should do this more nicely!

        Note: using pyephem is not very nice (marked as deprecated). Better use astropy/jplephem,
        but need to treat case where date moves outside the defined valid range...
        No need for accuracy in this application, but reasonable and stable results needed.
        """

        if body == "Proxima Centauri b":
            # http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=Proxima+Centauri&submit=SIMBAD+search
            #from astropy import units as u
            #from astropy.coordinates import SkyCoord
            #from astropy.time import Time
            #c = SkyCoord(ra="14h29m42.9451234609s", dec="-62d40m46.170818907s", distance=1/768.5004e-3*u.pc, frame='icrs')
            #c.representation_type = "cartesian"
            #print(c.transform_to(astropy.coordinates.HeliocentricMeanEcliptic))
            #hlon = 239.11463626 #deg
            #hlat = -44.76332445 #deg
            #distance = 1.30123548 #pc
            x = self.spherical_to_cartesian(239.11463626, -44.76332445, 1.30123548 * 206265)

        elif body == "Earth":
            # b is "Sun" object and gives heliocentric Earth location
            b = ephem.Sun()
            b.compute(date)
            x = self.spherical_to_cartesian(b.hlon, b.hlat, b.earth_distance)
        elif body == "Moon":
            # Note: hlon, hlat are geocentric for Moon!
            b = ephem.Moon()
            b.compute(date)
            xm = self.spherical_to_cartesian(b.hlon, b.hlat, b.earth_distance)
            s = ephem.Sun()  # Note: this is Earth heliocentric location
            s.compute(date)
            xe = self.spherical_to_cartesian(s.hlon, s.hlat, s.earth_distance)
            x = xe + xm
        elif body == "Mars":
            b = ephem.Mars()
            b.compute(date)
            x = self.spherical_to_cartesian(b.hlon, b.hlat, b.sun_distance)
        elif body == "Jupiter":
            b = ephem.Jupiter()
            b.compute(date)
            x = self.spherical_to_cartesian(b.hlon, b.hlat, b.sun_distance)
        elif body == "Saturn":
            b = ephem.Saturn()
            b.compute(date)
            x = self.spherical_to_cartesian(b.hlon, b.hlat, b.sun_distance)
        else:
            # object not known: just use Sun to prevent failure
            Logger.error("Space: Body '{}' not known!".format(body))
            x = np.array([0., 0., 0.])

        Logger.debug("Space: get_heliocentric_loc({}) = {}".format(body, x))
        return x


    def spherical_to_cartesian(self, longitude, latitude, distance):
        """Convert spherical coordinates to cartesian coordinates. Longitude and latitude given in radians."""
        Logger.debug("Space: spherical_to_cartesian long={} lat={} dist={}".format(longitude, latitude, distance))
        return distance * np.array([np.cos(latitude) * np.cos(longitude), 
                                    np.cos(latitude) * np.sin(longitude), 
                                    np.sin(latitude)])





#astropy.coordinates.get_body_barycentric("Earth", astropy.time.Time("2019-07-04T00:00:00", format="isot", scale="utc"))
