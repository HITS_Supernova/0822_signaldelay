# -*- coding: utf-8 -*-

""" global configuration variables """

from __future__ import print_function, unicode_literals
from __future__ import division   # absolute_import

import numpy as np
import textwrap
from kivy.logger import Logger

# external hilbert-heartbeat library should be reachable via PYTHONPATH
try:
    import heartbeat
    heartbeat_imported = True
except:
    heartbeat_imported = False
    Logger.warning("CFG: Import of Hilbert heartbeat library unsuccessful. Continuing without heartbeat.")



### globally accessible objects:

# parsed command line arguments and program name
args = None
progname = None

# communication
radio = None

# GUI root widget
root = None

# action class (takes care of handling action events, high-level)
action = None

# communication terminal number (to identify the participants)
station = None

# sound
use_sound = True

# send message when pressing "Enter" key
enter_sendman = True

soundfile = {"send": "sound/beep-quindar.wav", "receive": "sound/beep-beep.wav" }

languages_available = ["de", "en"]
lang = languages_available[0]

# plustime: taken care of by StationClock widget
# use .add_plustime(...) to change
#
# "advanced" time (due to signal delay, added to local wallclock time)
# - set to zero on start or reset (i.e. set local time)
# - normally does not advance
# - jumps according to message transfer (signal travel time)
clock = None

# interval between gui clock updates
clock_update_interval = 0.1

# message origin is colored
message_color_local = "#FFFF00"
message_color_remote = "#00FFFF"


# duration of transmission animation, in secs
animation_duration = 1.3

# location: local and remote station
local = "Earth"
remote = "Moon"


locations = {
    "Earth": { "en": "Earth", "de": "Erde", "image": "img/earth.jpg"},
    "Moon": { "en": "Moon", "de": "Mond", "image": "img/moon.jpg"},
    "Mars": { "en": "Mars", "de": "Mars", "image": "img/mars.jpg"},
    "Jupiter": { "en": "Jupiter", "de": "Jupiter", "image": "img/jupiter.jpg"},
    "Saturn": { "en": "Saturn", "de": "Saturn", "image": "img/saturn.jpg"},
    "Proxima Centauri b": { "en": "Proxima Centauri b", "de": "Proxima Centauri b", "image": "img/proxima_b.jpg"},
    }
locations_list = ["Earth", "Moon", "Mars", "Jupiter", "Saturn", "Proxima Centauri b"]

location_chooser_title = {
    "de": "NEUES ZIEL WÄHLEN:",
    "en": "SELECT NEW DESTINATION:",
    }
help_title = {
    "de": "ÜBER DIESE STATION",
    "en": "ABOUT THIS STATION",
    }
help_button_text = {
    "de": "SCHLIESSEN",
    "en": "CLOSE",
    }
help_text = {
    "de": textwrap.dedent('''\
        [i]Schreiben Sie eine Nachricht und beobachten Sie, wie lange es dauert bis die Nachricht ihr Ziel erreicht![/i]

        - Schreiben Sie eine Nachricht auf der Tastatur.
        - Drücken Sie "SENDEN" um die Nachricht abzusenden (oder drücken Sie die Eingabetaste).
            Beobachten Sie auf der Uhr, wie die Zeit während der Übertragung vergeht.
        - Drücken Sie "ZIEL" um ein anderes Ziel für die Nachricht auszuwählen.
        - Drücken Sie "RESET" um die vorherigen Nachrichten zu löschen und die Uhr auf die aktuelle Zeit zurückzusetzen.
        - Wenn jemand auf Ihre Nachricht an der anderen Communicator-Station antwortet, werden Sie diese Nachricht auch erhalten.
        '''),
    "en": textwrap.dedent('''\
        [i]Enter a message and see how long it takes to reach its destination![/i]

        - Type your message with the keyboard.
        - Press "SEND" to send it (or hit the "Return" key). Check the clock to see, how time passes during the transmission.
        - Press "DEST" to select another destination.
        - Press "RESET" to delete previous messages and set the clock back to present time.
        - If someone answers your message on the opposite communicator station, you will receive their messages, too.
        '''),
    }
credit_text = "\n\n\n\n\n\n\n" + textwrap.dedent('''\
    [size=18]
    [i]Credits:[/i]
    Earth image: NASA/USGS EROS/MODIS/Robert Simmon/Reto Stöckli, public domain -- Moon image: Luc Viatour, "Full Moon Luc Viatour", CC BY-SA 3.0 -- Mars image: NASA/JPL/MSSS, public domain -- Jupiter image: NASA/JPL/USGS, public domain -- Saturn image: Mattias Malmer/Cassini Imaging Team (NASA), public domain -- Proxima b image (artistic rendering): ESO, "Screenshot of ESOcast 87", CC BY 4.0 -- Sound includes "Quindar tones" audio by Benscripps on Wikimedia Commons, CC BY-SA 3.0 -- URLs and details given in the README.md at https://gitlab.com/HITS_Supernova/0822_signaldelay.
    [/size]
    ''')

# hilbert heartbeat
#
# set heartbeat_enabled to True if you want to use Hilbert heartbeat functionality for monitoring the program health
heartbeat_enabled = True

if heartbeat_enabled and heartbeat_imported:
    heartbeat_active = True
    heartbeat_startup_sec = 30.   # seconds
    heartbeat_interval_sec = 10.  # seconds
    heartbeat_shutdown_sec = 10.  # seconds
    heartbeat_first_ping_shown = False
else:
    heartbeat_active = False


Logger.warning("CFG: registering stuff...")
# register font and variants for markup (otherwise doesn't find them)
from kivy.resources import resource_add_path
from kivy.core.text import LabelBase
resource_add_path("./fonts/")
LabelBase.register("RobotoMono",
                    fn_regular="RobotoMono-Regular.ttf",
                    fn_italic="RobotoMono-RegularItalic.ttf",
                    fn_bold="RobotoMono-Bold.ttf",
                    fn_bolditalic="RobotoMono-BoldItalic.ttf")


# converter function, globally available in cfg
def datetime64_to_string(t, integer_seconds=False):
    """Helper: Convert datetime64 to a formatted string.

    If numpy array is given, it returns a list of strings.
    "integer_seconds": give accuracy in seconds only (instead of milliseconds).
    """

    # convert to date with simple strftime:
    # s = t.astype(datetime.datetime).strftime("%Y-%m-%d\n%H:%M:%S")
    #
    # more stable, works until years ~100 000:
    # also works on list of times
    # unit="s" second, "ms" millisecond accuracy
    s = np.datetime_as_string(t, unit="ms", timezone="naive")
    if integer_seconds:
        return s[:-4]
    else:
        return s

def string_to_datetime64(s):
    """Back to time variable, ms units enforced."""
    return np.datetime64(s, "ms")

