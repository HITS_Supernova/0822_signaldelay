# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals
from __future__ import division   # absolute_import

from kivy.logger import Logger

import cfg
import json
import numpy as np
import textwrap

import space
from commgui import HelpPopup
from locationchooser import LocationChooser
from helpscreen import HelpScreen



class Action(object):
    """Actions that get triggered by user input (via widgets), or automatically).

    tbd.
    """

    def __init__(self):
        # object to do all the space distance calculations
        self.space = space.Space()


    #########################################################################################
    ### message actions
    #
    #
    # these are triggered by receiving or sending messages (sending and receiving are treated alike!)
    # take care of state changes or gui update calls


    def connection(self, proto):
        Logger.info("Action: >>> connection")

        # when connection is made:
        # - server tells current plustime for synchronization
        # - client receives and sets sync'ed plustime (but as "message" action)


        if cfg.args.server:
            # server publishes authoritative plustime value on new connect
            self.sync_plustime()

            # server publishes location assignment
            self.sync_location()


        cfg.root.set_footer("SUPERNOVA CONSOLE #{}".format(cfg.radio.factory.station_id))


    def message(self, msg):
        """Transmission / notification just published (sent OR received)."""

        # local origin?
        from_local = msg.get("from_local")

        # IMPORTANT: keep this info logging of messages, to have a collection of messages during testing in the logs in case of trouble.
        if from_local:
            s = "received"
        else:
            s = "sent"
        Logger.info("Action: message ({}): {}".format(s, msg))

        if "sync_plustime" in msg:
            if from_local:
                Logger.info("Action: not sync_plustime because we sent it.")
            else:
                cfg.clock.sync_plustime(msg["sync_plustime"])

        if "new_location" in msg:
            # new location message authoritatively sets location of "server" and "client"
            if cfg.args.server:
                local = "server"
                remote = "client"
            else:
                local = "client"
                remote = "server"
            cfg.local = msg["new_location"][local]
            cfg.remote = msg["new_location"][remote]
            Logger.info("Action: Location update local = {}, remote = {}".format(cfg.local, cfg.remote))

            cfg.root.update_commlink()
            cfg.root.update_imagebox()

        if "message" in msg:
            plustime = msg["plustime"]

            #if plustime > 0:
            # increment time and start animation
            cfg.clock.add_plustime(plustime)
            cfg.root.ids.comm_link.animate_message_to("remote" if from_local else "local")

            # sanitize message text
            m = self.sanitze_string(msg["message"].rstrip())
            msg["message"] = m
            #if len(m) > 0:
            # display in widget
            cfg.root.add_message(msg)
            #else:
            #    Logger.warning("Action: message without text.")   # probably ok for many...


        if msg.get("attention"):
            Logger.info("Action: ATTENTION message received")
            # TODO: play sound!


    #########################################################################################
    ### widget actions:
    #
    # triggered by widgets, e.g. button press, ...


    def widget_send(self, input_widget, message=None, attention=False):
        # input_widget.text is the content unless message argument is used

        if message:
            text = message
        else:
            text = input_widget.text.strip()
            input_widget.text = ""  # clear input widget

        if not text:
            Logger.info("Action: send triggered, but text empty. Ignore.")
            return

        dt = self.space.get_signal_time(source=cfg.local, destination=cfg.remote, date=cfg.clock.text)
        msg = { "plustime": dt,
                "message": text,
                "attention": attention,
                "sender": cfg.local,
                "sendtime": cfg.datetime64_to_string(cfg.clock.t),
                "receivetime": cfg.datetime64_to_string(
                    cfg.clock.t + np.timedelta64(int(1000. * dt), "ms")),
                "receiver": cfg.remote }
        cfg.radio.publish(msg)


    def widget_attention(self):
        Logger.info("Action: attention called.")
        self.widget_send(None, message="<ATTENTION>", attention=True)


    def widget_location(self):
        Logger.info("Action: new_location called.")

        # Currently, this widget is only shown for the server, and hidden on the client.
        # (see communicator.kv). This should make it more understandable for users.
        # However, with the respective button exposed, also the client could set the server location.

        # !!! this defines what "pressing LOC" actually does: change local or remote location
        # !!! also check message() method and how it interprets it (remotely!)
        # --> update remote station

        l = LocationChooser(title=cfg.location_chooser_title[cfg.lang], chosen_callback=self.select_location)
        cfg.root.add_widget(l)


    def select_location(self, choice):
        """Callback method to select a new location. Used by LocationChooser."""
        Logger.info("Action: Set new location to {}".format(choice))
        cfg.remote = choice
        self.sync_location()


    def widget_reset(self):
        Logger.info("Action: reset called.")
        cfg.clock.reset_plustime()
        self.sync_plustime()  # permit client to reset time, too
        cfg.root.reset()


    def widget_language(self, lang_widget, lang):
        cfg.lang = lang
        cfg.root.update_texts()
        Logger.info("Action: lang = {} called.".format(lang))

        # set next language in widget text
        lang_widget.text = cfg.root.next_language().upper()


    def widget_help(self):
        """Show help and credits."""

        hs = HelpScreen(title=cfg.help_title[cfg.lang],
                        help_text=cfg.help_text[cfg.lang]+cfg.credit_text,
                        button_text=cfg.help_button_text[cfg.lang])
        cfg.root.add_widget(hs)


    #########################################################################################
    ### helpers
    #

    def sync_plustime(self):
        """Synchronize my (local) plustime as authoritative.

        This is done by server when a client connects. But also done when any station resets 
        its plustime to 0, hence needs to keep the other in sync.
        """

        my_plustime = cfg.clock.get_plustime()
        cfg.radio.publish({ "sync_plustime": my_plustime })


    def sync_location(self):
        """Synchronize / publish my location settings as authoritative."""

        if cfg.args.server:
            local = "server"
            remote = "client"
        else:
            local = "client"
            remote = "server"

        msg = { "new_location": { local: cfg.local, remote: cfg.remote } }
        cfg.radio.publish(msg)


    def sanitze_string(self, s):
        """Not sanitizing here - it's now done by the input widget CommInput in commgui.py!"""

        """Sanitize string to be non-markup, because markup is added by client."""
        #import re
        #Logger.info("sanitize: {}".format(s))
        ## string.printable as basis, some removed
        #return re.sub('[^0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZäöüÄÖÜß!"#$%&\'()][*+,-./:;<=>?@^_{|}~ \t\n\r]', "", s)
        return s


    def format_message(self, s, from_local):
        """Format message with markup.

        TODO: If label has specific font file given which does not include bold/italic,
        using those commands does not work.
        need to see register() in /usr/lib/python2.7/dist-packages/kivy/core/text/__init__.py:
        register with alias, and then it should know.!!!
        """

        if from_local:
            #ret = "[i]{}[/i]".format(s)
            ret = "[color=#FFFF00]{}[/color]".format(s)
        else:
            #ret = "[b]{}[/b]".format(s)
            ret = "[color=#0000FF]{}[/color]".format(s)
        Logger.info("format_message: {}  {}".format(ret, from_local))
        return ret

    def format_status_message(self, s):
        """Format status message with markup."""
        return "[color=#707070]{}[/color]".format(s)




