"""
    StationClock
    ============


"""

from kivy.lang            import Builder
from kivy.uix.label       import Label
from kivy.properties      import StringProperty, ListProperty,\
                                 BooleanProperty, NumericProperty, ObjectProperty
from kivy.clock           import Clock
from kivy.logger          import Logger
import numpy as np
import datetime
import time

import cfg


Builder.load_string('''

<StationClock>:
    halign: "center"
    valign: "center"
    markup: True

    canvas.before:
        Color:
            rgba:
                root.frame_color
        BorderImage:
            border: 59, 59, 59, 59
            source: "img/borderbox.png"
            size: self.size
            pos: self.pos
    #Label:
    #    size: root.size
    #    pos: root.pos
    #    #text: "[b]" + root.text + "[/b]"
    #    color: root.text_color
    #    font_size: root.font_size
    #    markup: True
    #    text_size: self.size
    #    halign: "center"
    #    valign: "center"
''')


class StationClock(Label):
    """Class for a Clock widget that supports "plustimes".

    Clock is animated by itself (updates its time).
    Plustimes are added by "add_plustime" method. This also triggers the corresponding highlighting and animation.

    plustime: numpy timedelta64 that takes care of incrementing time.
        Using ms resolution to be able to reach +/- 2.9e8 years.
        Change by using add_plustime() method!
    """

    text = StringProperty("n/a")
    """Text content."""

    highlighted = BooleanProperty(False)
    """Widget highlighted?"""

    font_size = NumericProperty(20)
    """Font size of the labels. (Inherited from Label class)."""

    font_size_high = NumericProperty(25)
    """Font size of the labels when highlighted."""

    font_size_low = NumericProperty(20)
    """Font size of the labels when not highlighted."""

    color = ListProperty([0, 0, 1, 1])
    """rgba color of the label text. (Inherited from Label class)."""

    text_color_high = ListProperty([1, 1, 0, 1])
    """rgba color of the label text when highlighted."""

    text_color_low = ListProperty([0.8, 0.8, 0, 1])
    """rgba color of the label text when not highlighted."""   

    frame_color = ListProperty([1, 1, 0, 1])
    """Canvas background rgba color."""

    frame_color_low = ListProperty([0.8, 0.8, 0, 1])
    """Frame rgba color when not highlighted."""

    frame_color_high = ListProperty([1, 1, 0, 1])
    """Frame rgba color when highlighted."""

    update_interval = NumericProperty(0.1)
    """Interval for time update."""

    date_time_separator = StringProperty("  ")
    """String separator between date and time for formatted string."""

    t = np.datetime64(datetime.datetime.now(), "ms")
    """Current time as np.datetime64() variable. Updated along with self.text."""

    def __init__(self, **kwargs):
        Logger.debug("StationClock: init")
        super(StationClock, self).__init__(**kwargs)

        # register reference to self in cfg for easy accessibility from other code parts
        cfg.clock = self

        self.highlighted = False
        Logger.warning("StationClock: col={} low={} high={}".format(self.frame_color, self.frame_color_low, self.frame_color_high))

        self.plustime = np.timedelta64(0, "ms")

        # pending plustime steps (for animation)
        self._pending_plustimes = []
        self._clock_animation = None


        # regular time update
        self._clock_event = None
        self.set_regular_update()
        #Clock.schedule_once(self.mess_up_timer, 5.)


    def add_plustime(self, seconds, steps=111, duration=cfg.animation_duration):
        """Increase the plustime. Takes care of animation, too."""

        Logger.debug("StationClock: add_plustime {} seconds in {} steps".format(seconds, steps))

        dt = np.timedelta64(int(1000. * seconds / steps), "ms")
        Logger.debug("StationClock: dt = {}".format(repr(dt)))

        if steps > 1:
            self._pending_plustimes.extend(steps * [dt])
        else:
            self._pending_plustimes.append(dt)

        # animation starts
        self.highlighted = True
        #Logger.info("StationClock: pending = {}".format(self._pending_plustimes))
        if not self._clock_animation:
            self._clock_animation = Clock.schedule_interval(self._process_pending_plustimes, 1.*duration/steps)

    def get_plustime(self):
        """Return current plustime in seconds."""
        return self.plustime.item().total_seconds()

    def reset_plustime(self):
        """Set plustime to zero.

        Could also set to zero directly, but without add_plustime there is no animation.
        """
        Logger.info("StationClock: Resetting plustime to zero ...")
        #self.add_plustime(-1. * self.get_plustime())
        self.plustime = np.timedelta64(0, "ms")

    def sync_plustime(self, seconds):
        """Synchronizes plustime to the remote value (sent by the server."""

        Logger.info("StationClock: Sync plustime to remote value of {} seconds.".format(seconds))
        self.plustime = np.timedelta64(int(1000. * seconds), "ms")

    def _process_pending_plustimes(self, dt):
        """Processed regularly by clock scheduler for animation:

        Add next plustime step and trigger time string update."""
        if self._pending_plustimes:
            #Logger.info("StationClock: animation running ...  {}".format(self._pending_plustimes))
            self.plustime += self._pending_plustimes.pop(0)
            self.update_time_string()
        else:
            Logger.info("StationClock: Animation done.")
            Logger.info("StationClock: Current plustime = {}".format(repr(self.plustime)))
            self._clock_animation.cancel()
            self._clock_animation = None
            self.highlighted = False

    def on_highlighted(self, *args):
        if self.highlighted:
            # enable highlight
            self.font_size = self.font_size_high
            self.color = self.text_color_high
            self.frame_color = self.frame_color_high
        else:
            # disable highlight
            self.font_size = self.font_size_low
            self.color = self.text_color_low
            self.frame_color = self.frame_color_low
        Logger.debug("StationClock: ******** highlighted({}) -> frame_color={} text_color={}".format(self.highlighted, self.frame_color, self.color))

    def on_frame_color_high(self, *args):
        self.on_highlighted()

    def on_frame_color_low(self, *args):
        self.on_highlighted()

    def update_time_string(self, dt=None):
        self.t = np.datetime64(datetime.datetime.now(), "ms") + self.plustime

        # a little replace tweak for text representation, no millisecond precision
        self.text = cfg.datetime64_to_string(
            self.t, integer_seconds=True).replace("T", self.date_time_separator)

        Logger.debug("StationClock: self.t = {}".format(self.t))

    def set_regular_update(self):
        Logger.debug("StationClock: set_regular_update")
        self._clock_event = Clock.schedule_interval(self.update_time_string, self.update_interval)

    def cancel_regular_update(self):
        Logger.debug("StationClock: cancel_regular_update")
        if self._clock_event:
            self._clock_event.cancel()

    def on_update_interval(self, clock_object, value):
        Logger.info("StationClock: on_update_interval clock={} value={}".format(clock_object, value))
        self.cancel_regular_update()
        self.set_regular_update()

    #def mess_up_timer(self, dt):
    #    Logger.info("StationClock: mess_up_timer")
    #    self.update_interval = 1.


if __name__ == "__main__":
    from kivy.app import App

    class SampleApp(App):
        def build(self):
            return StationClock()

    SampleApp().run()

