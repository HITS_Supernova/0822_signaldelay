FROM hits/hitsfat:devel_0.10.0-1

ARG DP_CREDENTIALS=none

WORKDIR /I
RUN /usr/local/bin/update.sh \
    && /usr/local/bin/install.sh xclip
# quick fix: update Twisted to deal with ancient python3 ...
RUN . /opt/venv3/bin/activate && pip install --upgrade pip && pip install Twisted==19.2.1 && pip install pyephem==3.7.6.0
COPY hilbert-heartbeat /I/hilbert-heartbeat/
COPY Xmodmap-restricted /I/Xmodmap-restricted
COPY Python /I/Python/
COPY start.sh /I/start.sh
#COPY kivy_config.ini /root/.kivy/config.ini
#COPY 2in1.sh /I/

ARG IMAGE_VERSION=latest
ARG GIT_NOT_CLEAN_CHECK
ARG BUILD_DATE=unknown
ARG VCS_REF=unknown
ARG VCS_DATE=unknown
ARG VCS_SUMMARY=unknown
ARG VCS_URL=unknown
ARG DOCKERFILE=unknown

LABEL maintainer="Volker Gaibler <volker.gaibler@h-its.org>" \
    org.label-schema.name="Signal Delay" \
    org.label-schema.description="Interactive application for the ESO Supernova" \
    org.label-schema.vendor="HITS gGmbH" \
    org.label-schema.vcs-ref="${VCS_REF}" \
    org.label-schema.vcs-url="${VCS_URL}" \
    org.label-schema.version="${VCS_VERSION}" \
    org.label-schema.build-date="${BUILD_DATE}" \
    org.label-schema.schema-version="1.0" \
    VCS_DATE="${VCS_DATE}" \
    VCS_SUMMARY="${VCS_SUMMARY}" \
    IMAGE_VERSION="${IMAGE_VERSION}" \
    GIT_NOT_CLEAN_CHECK="${GIT_NOT_CLEAN_CHECK}" \
    DOCKERFILE="${DOCKERFILE}"
