# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals
from __future__ import division   # absolute_import

import re
import time
import numpy as np

from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.behaviors.focus import FocusBehavior
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from kivy.properties import StringProperty, ListProperty
from kivy.logger import Logger
from kivy.clock import Clock

from stationclock import StationClock
from buttonbox import BorderButton, BarButton, FillButton, BorderLabel, BarLabel, FillLabel
from commlink import CommLink
from messagelist import MessageList


# local configuration
import cfg



###############################################################################
class CommGui(FloatLayout):
    """The root widget"""

    statusbar_text = StringProperty("")

    def __init__(self, **kwargs):

        # widget init
        super(CommGui, self).__init__(**kwargs)

        #self.last_activity = time.time()
        #print("TCRoot __init__ done.")

        self.update_commlink()
        self.update_imagebox()
        self.update_texts()


    def add_message(self, msg):
        # let message widget handle it
        self.ids.message_list.add_message(msg)

    def update_commlink(self):
        """Update the communication link widget, using the current location settings."""

        Logger.info("CommGui: update_commlink {} -> {}".format(cfg.local, cfg.remote))
        self.ids.comm_link.update()


    def update_imagebox(self):
        """Update the image box that shows the object we're sending to / receiving from."""
        Logger.info("CommGui: update_screenbox  {}".format(cfg.remote))
        self.ids.imagebox.img_source = self.image_path_from_location(cfg.remote)

    def set_footer(self, text):
        self.ids.footer.text = text

    def image_path_from_location(self, location):
        if location in cfg.locations:
            return cfg.locations[location]["image"]
        else:
            return ""

    def reset(self):
        self.ids.message_list.reset()
        self.ids.comm_input.reset_text()


    def update_texts(self):
        """Update all language-related texts in the root and its child widgets."""
        Logger.debug("Root: update_texts()")
        self.ids.message_list.update_text()
        self.ids.comm_link.update()
        for wid in ["button_help", "button_target", "button_reset", "button_attention", "button_send"]:
            self.ids[wid].text = self.ids[wid].text_lang[cfg.lang]

    def current_language(self):
        return cfg.lang


    def next_language(self):
        ind = cfg.languages_available.index(self.current_language())
        next_lang = cfg.languages_available[(ind + 1) % len(cfg.languages_available)]
        return next_lang

###############################################################################
class CommInput(TextInput, FocusBehavior):
    """Customized text input widget."""

    def __init__(self, **kwargs):
        super(CommInput, self).__init__(**kwargs)
        self.copy(" ")  # clear clipboard

    def keyboard_on_key_up(self, window, keycode):
        # temporarily logging as info instead of debug:

        # Send on "Enter"?
        if cfg.enter_sendman and keycode[0] == 13:  # enter key
            Logger.info("CommInput: Send by Enter.")
            #cfg.action.widget_send(self)
            cfg.root.ids.button_send.trigger_action()

        Logger.info("CommInput: keyboard_on_key_up: keycode {}. cursor={}".format(
            keycode, self.cursor))
        if keycode[0] == 27:  # escape key
            return True  # handled it

        super(CommInput, self).keyboard_on_key_up(window, keycode)
        return True  # consumed key

    def on_parent(self, widget, parent):
        """Initialize focus _after_ visible.

        https://kivy.org/docs/api-kivy.uix.behaviors.focus.html#initializing-focus
        """
        Logger.info("CommInput: on_parent")
        self.focus = True

    def on_focus(self, instance, value):
        """Keep focus? "Escape" makes it lose focus and not get it back....

        This is a hack - forcing focus should be possible in a nicer way!
        Also need to avoid app exiting on escape (if it ever manages to received an escape)
        OR: xmodmap to disable escape on the OS level for exhibition
        """

        if value:
            Logger.debug("CommInput: received focus: {} {}".format(instance, value))
        else:
            Logger.debug("CommInput: lost focus: {} {}".format(instance, value))
            from kivy.clock import Clock
            def focus_it():
                self.focus = True
            Clock.schedule_once(lambda dt: focus_it(), 0)  # re-focus after next frame


    # replace input text (filter!)
    def insert_text(self, substring, from_undo=False):
        """Sanitize input text."""
        #s = substring.upper()
        #s = substring
        s = re.sub('[^0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZäöüÄÖÜß!"#$%&\'()*+,-./:;<=>?@^_{|}~ \t\n\r]', "", substring)
        #Logger.info("insert_text")
        return super(CommInput, self).insert_text(s, from_undo=from_undo)

    def reset_text(self):
        self.text = ""


###############################################################################
class ActionButton(Button):
    """Customized button to trigger an action."""
    pass
    #color_current = ListProperty([1, 1, 1, 1])
    """Color property that defines the tinting of the BorderImage and the text."""


###############################################################################
class HelpPopup(Popup):
    """Info popup that shows help and credits."""
    pass




