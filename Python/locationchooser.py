# -*- coding: utf-8 -*-

"""
    LocationChooser
    ===============


"""

from __future__ import print_function, division, unicode_literals

from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.floatlayout import FloatLayout

from kivy.lang            import Builder
from kivy.uix.label       import Label
from kivy.properties      import StringProperty, ListProperty,\
                                 BooleanProperty, NumericProperty, ObjectProperty
from kivy.clock           import Clock
from kivy.logger          import Logger
import numpy as np
import datetime
import time

import cfg

from buttonbox import FillButton


Builder.load_string('''

<LocationElement>:
    orientation: "vertical"

    Image:
        id: img
        source: root.source

    FillButton:
        id: button
        text: root.text
        height: 120
        size_hint: 1, None

<LocationList>:
    cols: 3
    rows: 2
    # padding between layout box and its children:
    padding: 100
    # spacing between children:
    spacing: 25, 100


<LocationChooser>:
    halign: "center"
    valign: "center"
    markup: True
    title_color: 1, 1, 1, 1
    title_fontsize: 40

    canvas.before:
        Color:
            rgba:
                0, 0, 0, 1
        Rectangle:
            size: root.width, root.height * (1 - 2 * root.grid_block_height)
            pos: self.x, root.height * root.grid_block_height

        Color:
            rgba:
                root.frame_color
        BorderImage:
            border: 59, 59, 59, 59
            source: "img/borderbox.png"
            size: root.width, root.height * (1 - 2 * root.grid_block_height)
            pos: self.x, root.height * root.grid_block_height

    Label:
        id: title_id
        text: root.title
        color: root.title_color
        pos_hint: { "x": 0.05, "y": 1. - 10 * root.grid_block_height }
        size_hint: 0.9, 6. * root.grid_block_height
        font_size: root.title_fontsize

    LocationList:
        id: list_id
        text: "CONTENT"
        pos_hint: { "x": 0.05, "y": 4. * root.grid_block_height }
        size_hint: 0.9, 42 * root.grid_block_height

''')



class LocationElement(BoxLayout):

    label = StringProperty("label")
    text = StringProperty("text")
    source = StringProperty("img/earth.jpg")
    state = StringProperty("normal")

    def press(self):
        """Presses the button, even if only image was touched."""
        self.state = "down"
        self.ids.button.state = "down"

    def release(self):
        """Release the button."""
        self.state = "normal"
        self.ids.button.state = "normal"

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            touch.grab(self)
            self.press()
            return True

    def on_touch_up(self, touch):
        if touch.grab_current is self:
            touch.ungrab(self)
            self.release()
            return True


class LocationList(GridLayout):

    choice = StringProperty("")

    def __init__(self, **kwargs):

        super(LocationList, self).__init__(**kwargs)

        # TODO: generalize / move to outside!
        self.locations = cfg.locations
        self.locations_list = cfg.locations_list

        for l in self.locations_list:
            e = LocationElement(label=l,
                                text=self.locations[l][cfg.lang].upper(),
                                source=self.locations[l]["image"])
            e.bind(state=self.choice_made)
            self.add_widget(e)


    def choice_made(self, widget, value):
        """Callback when a choice was made."""

        if value == "normal":
            # only report "choice-made" when touch is released
            Logger.info("LocationList: choice_made label={}".format(widget.label))
            self.choice = widget.label


class LocationChooser(FloatLayout):

    chosen_callback = ObjectProperty(None)
    """Reference to callback function that should be notified / deal with the new choice."""

    grid_block_height = NumericProperty(1/54.)
    """Grid scaling defined externally, e.g. 1/nblocky."""

    title = StringProperty("title")
    """Title text."""

    font_size = NumericProperty(20)
    """Font size of the labels. (Inherited from Label class)."""

    color = ListProperty([0, 0, 1, 1])
    """rgba color of the label text. (Inherited from Label class)."""

    frame_color = ListProperty([1, 1, 0, 1])
    """Canvas background rgba color."""

    def __init__(self, **kwargs):
        super(LocationChooser, self).__init__(**kwargs)
        self.ids.list_id.bind(choice=self.choice_changed)

    def choice_changed(self, widget, value):
        """Internal callback to deal with the choice being set or modified in the LocationList child."""
        Logger.info("LocationChooser: choice has changed to '{}'".format(value))
        if self.chosen_callback:
            Logger.debug("LocationChooser: invoking callback")
            self.chosen_callback(value)
        else:
            Logger.debug("LocationChooser: no callback given. Doing nothing.")
        # dismiss popup right after choice was made
        self.dismiss()

    def dismiss(self):
        """Remove self."""
        Logger.debug("LocationChooser: ask parent to dismiss me")
        self.parent.remove_widget(self)


if __name__ == "__main__":
    from kivy.app import App

    class SampleApp(App):
        def build(self):
            return LocationChooser()
            #return LocationList()
            #return LocationElement()

    SampleApp().run()
